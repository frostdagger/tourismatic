angular
    .module('tour')
    .directive('countryArticle', countryArticle);

function countryArticle() {
    return {
        restrict: 'E',
        templateUrl: 'views/templates/country-article.html'
    }
}
