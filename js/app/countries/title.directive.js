angular
    .module('tour')
    .directive('countryTitle', countryTitle);

function countryTitle() {
    return {
        restrict: 'E',
        templateUrl: 'views/templates/country-title.html'
    }
}
