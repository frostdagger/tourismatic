angular
    .module('tour')
    .directive('countryStats', countryStats);

function countryStats() {
    return {
        restrict: 'E',
        templateUrl: 'views/templates/country-stats.html'
    }
}
