angular
    .module('tour')
    .directive('countryMap', countryMap);

function countryMap() {
    return {
        restrict: 'E',
        templateUrl: 'views/templates/country-map.html'
    }
}
