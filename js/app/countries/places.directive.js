angular
    .module('tour')
    .directive('countryPlaces', countryPlaces);

function countryPlaces() {
    return {
        restrict: 'E',
        templateUrl: 'views/templates/country-places.html'
    }
}

