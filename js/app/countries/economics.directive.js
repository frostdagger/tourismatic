angular
    .module('tour')
    .directive('countryEconomics', countryEconomics);

function countryEconomics() {
    return {
        restrict: 'E',
        templateUrl: 'views/templates/country-economics.html'
    }
}
