angular
    .module('tour')
    .directive('countryFacts', countryFacts);

function countryFacts() {
    return {
        restrict: 'E',
        templateUrl: 'views/templates/country-facts.html'
    }
}
