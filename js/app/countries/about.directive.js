angular
    .module('tour')
    .directive('countryAbout', countryAbout);

function countryAbout() {
    return {
        restrict: 'E',
        templateUrl: 'views/templates/country-about.html'
    }
}
