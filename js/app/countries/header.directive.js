angular
    .module('tour')
    .directive('countryHeader', countryHeader);

function countryHeader() {
    return {
        restrict: 'E',
        templateUrl: 'views/templates/country-header.html'
    }
}
