angular
    .module('tour')
    .directive('countryThumb', countryThumb);

function countryThumb() {
    return {
        restrict: 'E',
        templateUrl: 'views/templates/country-thumb.html'
    }
}
