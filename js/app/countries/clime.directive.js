angular
    .module('tour')
    .directive('countryClime', countryClime);

function countryClime() {
    return {
        restrict: 'E',
        templateUrl: 'views/templates/country-clime.html'
    }
}
