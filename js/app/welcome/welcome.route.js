angular
    .module('tour')
    .config(function ($routeProvider) {
        $routeProvider
            .when('/norway', {
                // redirectTo: '/norway.html',
                templateUrl: 'views/norway.html',
                controller: 'WelcomeCtrl'
            })
            .when('/moldova', {
                templateUrl: 'views/moldova.html',
                controller: 'WelcomeCtrl'
            })
            .when('/odessa', {
                templateUrl: 'views/moldova.html',
                controller: 'WelcomeCtrl'
            })
            .otherwise({
                redirectTo: 'index.html'
            })
    }); 
   