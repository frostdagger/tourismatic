angular
    .module('tour')
    .factory('getCountries', getCountries);

getCountries.$inject = ['$http'];

function getCountries($http) {
    
    return {
        getFile: getFile
    };

    function getFile() {
        var request = $http({
            method: 'GET',
            url: 'json/countries.json'
        });
        return (request.then(handleSuccess, handleError));
    }

    function handleSuccess(response) {
        return ( response.data );
    }

    function handleError(response) {
        return ( response.data );
    }

}


//
// getCountries.$inject = ['$http', '$scope'];
//
// function getCountries($http, $scope) {
//
//     return $http({
//         method: 'GET',
//         url: 'json/countries.json'
//     }).then(function success(response) {
//         $scope.continents = response.continents;
//     });
// }